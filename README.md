# AMAW - Automated MAKER2 Annotation Wrapper

## Aim and features

The pipeline devised in AMAW aims to reach three goals: (1) to achieve the most accurate annotation of a non-model genome without manual curation, (2) to automate the use of MAKER2 for supporting large-scale annotation projects, and (3) to simplify its installation and usage for users without a strong bioinformatics background.
 
First, a key factor for achieving accurate genome annotation is to collect as much evidence data (ESTs, transcripts and/or proteins) as possible. This is needed both to optimize the training of specific gene models of ab initio gene predictors and to improve the confidence level in predictions supported by experimental data (Holt et al., 2011).

Second, building evidence datasets is a time-consuming task, which also implies a certain level of bioinformatics skills. Indeed, this consists, in the best cases, to find and download directly available EST, transcript or protein datasets for the genome species to annotate. However, this process often further requires assembling raw RNA-Seq reads into transcripts and gathering a reasonably sized protein dataset, usually including sequences of taxa phylogenetically close to the organism of interest. If building evidence datasets is feasible for a few genomes to annotate, doing so repeatedly for dozens or hundreds of genomes is hardly conceivable. This is why AMAW addresses this issue by automating the acquisition of both available RNA-Seq and protein data from reliable public databases (“NCBI SRA” for RNA-Seq data and a combination of “Ensembl genomes” and NCBI databases for protein sequences).

Third, in addition of constructing a good input dataset for the annotation, AMAW automates the installation and the global use of the MAKER2 annotation pipeline based on good practices published by its authors (Campbell et al., 2015), and orchestrates the successive runs in a grid-computing environment. Even if MAKER2 is described as an easy to use pipeline, its handling and the optimal fine-tuning of its parameters demand to take notice of its large documentation and, again, require the user to have a good bioinformatics understanding.

The complete workflow of AMAW can be summarized in three steps:  
- Transcript evidence data acquisition: RNA-Seq acquisition, assembly into transcripts, quantification of the abundance of the transcripts and filtering of redundant transcripts and minor isoforms;  
- Protein evidence deployment;  
- MAKER2 iterative runs and progresive training of its internal gene predictors.  

It is possible for the user to provide her/his in-house protein and/or transcript dataset(s). Moreover, s/he can short-circuit the pipeline by choosing an existing gene model for AUGUSTUS and/or SNAP. However, unless available models are well-suited for the organism at hand (matching species), it is advised to rely on AMAW full analysis.

## Installation

Please first install Singularity v3: <https://sylabs.io/guides/3.0/user-guide/installation.html>

### Clone the repository and download auxiliary data

    $ git clone https://bitbucket.org/phylogeno/amaw/src/master/
    $ cd master
    $ wget https://figshare.com/ndownloader/files/31658918 -O taxdump.tgz
    $ tar -xzf taxdump.tgz
    $ wget https://figshare.com/ndownloader/files/31658921 -O prot_db.tgz
    $ tar -xzf prot_db.tgz

### Install dependencies

Download the following packages and place them in the current AMAW folder.
Make sure to use the exact versions listed below.

#### Trinity

Visit: <https://github.com/trinityrnaseq/trinityrnaseq/wiki>

    $ wget https://github.com/trinityrnaseq/trinityrnaseq/archive/refs/tags/Trinity-v2.4.0.tar.gz  

#### Bowtie 2

Visit: <https://github.com/BenLangmead/bowtie2>

    $ wget https://github.com/BenLangmead/bowtie2/releases/download/v2.3.5/bowtie2-2.3.5-linux-x86_64.zip

#### Jellyfish

Visit: <https://github.com/gmarcais/Jellyfish>

    $ wget https://github.com/gmarcais/Jellyfish/releases/download/v2.3.0/jellyfish-2.3.0.tar.gz

#### Salmon

Visit: <https://github.com/COMBINE-lab/salmon>

    $ wget https://github.com/COMBINE-lab/salmon/releases/download/v1.5.2/salmon-1.5.2_linux_x86_64.tar.gz

#### NCBI-BLAST+

Visit: <https://ftp.ncbi.nlm.nih.gov/blast/executables/blast+>

    $ wget https://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.10.0/ncbi-blast-2.10.0+-x64-linux.tar.gz

#### Tandem Repeats Finder

Visit: <https://tandem.bu.edu/trf/trf409.linux64.download.html>

Download the `trf409.linux64` version, without the limitation on `glibc`.

#### RMBlast

Visit: <http://www.repeatmasker.org/RMBlast.html>

    $ wget http://www.repeatmasker.org/rmblast-2.9.0+-p2-x64-linux.tar.gz

#### RepeatMasker

Visit: <https://www.repeatmasker.org/RepeatMasker/>

    $ wget https://www.repeatmasker.org/RepeatMasker/RepeatMasker-4.1.1.tar.gz

#### Exonerate

Visit: <https://www.ebi.ac.uk/about/vertebrate-genomics/software/exonerate>

    $ wget http://ftp.ebi.ac.uk/pub/software/vertebrategenomics/exonerate/exonerate-2.2.0-x86_64.tar.gz  

#### MAKER2

Visit: <https://www.yandell-lab.org/software/maker.html>

Register and download MAKER2.

### Build the container

Make sure to have root privileges on your system before building the container.  
Make sure to have all dependencies in your current directory before building the container; these files or directories should be present:

    Trinity-v2.4.0.tar.gz 
    bowtie2-2.3.5-linux-x86_64.zip 
    jellyfish-2.3.0.tar.gz 
    salmon-1.5.2_linux_x86_64.tar.gz 
    ncbi-blast-2.10.0+-x64-linux.tar.gz 
    trf409.linux64 
    rmblast-2.9.0+-p2-x64-linux.tar.gz 
    RepeatMasker-4.1.1.tar.gz 
    exonerate-2.2.0-x86_64.tar.gz 
    maker-2.31.11.tgz 
    amaw.pl 
    transcript-filter.pl 
    prot_db/
    convert_fathom2genbank.pl 
    align_and_estimate_abundance.pl

    $ sudo singularity build amaw.sif amaw.def

## Usage

Execute the container.
Make sure to bind directories (see <https://sylabs.io/guides/3.0/user-guide/bind_paths_and_mounts.html> for examples).
In the following lines, `<PATH-to-DIRECTORY>` corresponds to your local directory.
Additional files and directories (e.g., `taxdump`) are not meant to be included in the container but will be required when running AMAW.

Run AMAW:

	$ mkdir NCBI
	$ cd NCBI
	$ mkdir .ncbi
	$ cp <PATH-to-ncbi-setup>/user-settings.mkfg .ncbi/
	$ singularity exec --bind <PATH-to-AMAW-DB>:/temp,<PATH-to-DIRECTORY>/NCBI:${HOME},<PATH-to-DIRECTORY>:/mnt amaw.sif \
		amaw.pl --genome=/mnt/<FIELD2> \
		--organism=<FIELD3> --proteins=1 --est=1 \
		--taxdir=/temp/taxdump/ --maker-cpus=20 \
		--trinity-cpus=20 --rsem-cpus=20 \
		--augustus-db=/mnt/augustus-config/ \
		--outdir=/mnt/GENERA-annotation \
		--prot-dbs=/temp/prot_dbs/

`taxdump,`augustus-config`and prot_dbs`are the corresponding directories from this repository.

 prot-dbs: <https://doi.org/10.6084/m9.figshare.17124080>

 taxdump: <https://doi.org/10.6084/m9.figshare.17124074>

`augustus-config`can be updated from augustus GitHub: <https://github.com/Gaius-Augustus/Augustus>

## Pipeline functionalities and modes

In its basic usage, the pipeline can be launched with only the genome file and the organism name (formatted as "Genus\_species". This will by default perform the annotation with three MAKER2 runs and intermediate gene model training using the deployment of a phylogenetically-related database. The --est=1 option will perform the search of SRA RNA-Seq experiments and assemble these into transcripts which will be additionally used alongside the proteins. The protein and transcript datasets can also be provided with --protein-file and --est-file arguments. 

Further modes and usages of AMAW are implemented in order to shorten the annotation process to only one MAKER2 run:  
- --gm-db=<directory> automates the deployment of SNAP and AUGUSTUS gene models on the basis of the organism name (i.e., Genus_species) in order to facilitate large-scale analyses--augustus-gm and --snap-db can also be alternatively used to load AUGUSTUS or SNAP gene models.  
- --transcript-db=<directory> automates the deployment of pre-assembled transcripts based on the organism name (i.e., Genus_species). If a transcript file is available, it will be used, otherwise the search of RNA-Seq data is launched. 


![Description of AMAW pipeline modes and functionalities](./images/AMAW-pipeline.png)

## Copyright and License

This software is copyright (c) 2017-2021 by University of Liege / Sciensano / BCCM / Loic MEUNIER, Denis BAURAIN and Luc CORNET.
This is free software; you can redistribute it and/or modify.
